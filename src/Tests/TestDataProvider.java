package Tests;

import Core.ApiPrefix;
import Core.Component;
import Utils.ApiUtils;
import Utils.Configuration;
import Utils.TestData;
import Utils.TestDataReader;
import org.testng.annotations.DataProvider;

import java.util.*;

/**
 * Created by Admin on 28.09.2015.
 */
public class TestDataProvider
{
    @DataProvider(name = "dynamic")
    private static Iterator<Object[]> makeDynamic()
    {
        TestDataReader reader = new TestDataReader();
        List<TestData> testDataCollection = reader.GetTestData();
        ArrayList<Object[]> testData = new ArrayList<>();
        for (TestData data : testDataCollection) {
            String request = ApiUtils.Format(Configuration.BaseUrl, ApiPrefix.valueOf(data.ApiPrefix),
                    Component.valueOf(data.Component), data.Entity, data.Method, data.Data);
            testData.add(new Object[]{request, data.Expected, "Dynamic." + data.Entity + "." + data.Method + "(" + data.Data + ")"});
        }

        return testData.iterator();
    }

    @DataProvider(name = "apitests")
    private static Object[][] createData()
    {
        Object[][] allTests = new Object[][]
                {
                        PageAddNoTitle(), PageAddWrongAlias(), PageAdd(), PageAddWrongUrl(),
                        PageAddExternal(), PageAddHeading(), PageEdit(),PageEditNoId(),
                        PageEditWrongId(), PageEditTitle(), PageEditNoTitle(), PageEditSeo(), PageEditSeoWrongId(),
                        PageEditTitleWrongID(), PageEditTitleNoID(), PageEditTitleNoTitle(),
                        PageMoveNoId(), PageRemoveWrongId(), ModuleButtonAdd(), ModuleTitleAdd(), ModuleTitleAddRowID(), ModuleParagraphAdd(), ModuleFormAdd(),
                        ModuleImageAdd(), ModuleVideoAdd(), ColorPaletteAdd(), ColorPaletteAddRecent(), PageChangeMargin(),PageChangeMarginNoId(),
                };
        return allTests;
    }

    private static Object[] PageAddNoTitle()
    {
        return new Object[]{
                new String[][]
                        {
                                {"type", "-1", "type"},
                                {"title", "The title field is required.", "msg"}
                        },
                new HashMap<String, String>()
                {{
                        put("title", "");
                    }},
                "page", "add", "Page add no title",};
    }

    private static Object[] PageAddWrongAlias()
    {
        return new Object[]{
                new String[][]
                        {
                                {"type", "-1", "type"},
                                {"alias", "The alias may only contain letters, numbers, and dashes.", "msg"}
                        },
                new HashMap<String, String>()
                {{
                        put("title", "mytitle");
                        put("alias", "cv cv");
                    }},
                "page", "add", "Page add wrong alias"};
    }

    private static Object[] PageAdd()
    {
        return new Object[]{
                new String[][]
                        {
                                {"type", "1", "type"},
                                {"global", "Page saved", "msg"},
                                {"alias", "mysupertitle","data"},
                                {"title", "mysupertitle","data"},
                                {"target", "0","data"},
                                {"homepage", "0","data"},
                                {"parentPageId", "null","data"},
                                {"ordering", "2","data"},
                                {"url", Configuration.BaseUrl + "/mysupertitle","data"}
                        },
                new HashMap<String, String>()
                {{
                        put("title", "mysupertitle");
                        put("alias", "mysupertitle");
                        put("target", "0");
                        put("ordering", "2");
                        put("type", "pageBlank");
                    }},
                "page", "add", "Page add all data correct"};
    }

    private static Object[] PageAddWrongUrl()
    {
        return new Object[]{
                new String[][]
                        {
                                {"type", "-1", "type"},
                                {"url", "The url is not a valid URL.", "msg"}
                        },
                new HashMap<String, String>()
                {{
                        put("title", "titlewithwrongurl");
                        put("alias", "wrongurl");
                        put("url", "https://someilligalurl");
                        put("ordering", "3");
                        put("type", "pageExternal");
                    }}, "page", "add", "Page add wrong url"};

    }

    private static Object[] PageAddExternal()
    {
        return new Object[]{
                new String[][]
                        {
                                {"type", "1", "type"},
                                {"global", "Page saved", "msg"},
                                {"ordering", "3", "data"}
                        }, new HashMap<String, String>()
        {{
                put("title", "titlepageexternal");
                put("alias", "titlepageexternal");
                put("url", "https://external.com");
                put("ordering", "3");
                put("type", "pageExternal");
            }}, "page", "add", "PageAddExternal"};
    }

    private static Object[] PageAddHeading()
    {

        return new Object[]{new String[][]
                {
                        {"type", "1", "type"},
                        {"global", "Page saved", null},
                        {"ordering", "4", null}
                }, new HashMap<String, String>()
        {{
                put("title", "headpage");
                put("alias", "headpage");
                put("ordering", "4");
                put("type", "pageHeading");
            }}, "page", "add", "Page add with heading"};

    }

    private static Object[] PageEdit()
    {
        return new Object[]{
                new String[][]
                        {
                                {"type", "1", "type"},
                                {"global", "Page edited", "msg"},
                                {"url", Configuration.BaseUrl, "data"}
                        },
                new HashMap<String, String>()
                {{
                        put("title", "SomeNewTitle");
                        put("alias", "SomeNewTitle");
                        put("id", "1");
                    }}, "page", "edit", "Page Edit"};

    }

    private static Object[] PageEditNoId()
    {
        return new Object[]{
                new String[][]
                        {
                                {"type", "-1", "type"},
                                {"id", "The id field is required.", "msg"},
                        },
                new HashMap<String, String>()
                {{
                        put("title", "SomeNewTitle");
                        put("alias", "SomeNewTitle");
                    }}, "page", "edit", "Page Edit No Id"};
    }

    private static Object[] PageEditWrongId()
    {
        return new Object[]{
                new String[][]
                        {
                                {"type", "-1", "type"},
                                {"id", "The selected id is invalid.", "msg"}
                        },
                new HashMap<String, String>()
                {{
                        put("id", "999");
                        put("title", "SomeNewTitle");
                        put("alias", "SomeNewTitle");
                    }}, "page", "edit", "PageEditWrongId"};

    }

    private static Object[] PageEditNoTitle()
    {
        return new Object[]{
                new String[][]
                        {
                                {"type", "-1", "type"},
                                {"title", "The title field is required.", "msg"}
                        },
                new HashMap<String, String>()
                {{
                        put("id", "1");
                        put("alias", "SomeNewTitle");
                    }}, "page", "edit", "PageEditNoTitle"};
    }

    private static Object[] PageEditSeo()
    {
        return new Object[]{
                new String[][]
                        {
                                {"type", "1", "type"},
                                {"global", "Page edited", "msg"},
                                {"url", Configuration.BaseUrl, "data"},
                                {"pageTitle", "SomeNewSeoTitle", "data"},
                                {"description", "That is super title", "data"}
                        },
                new HashMap<String, String>()
                {{
                        put("pageTitle", "SomeNewSeoTitle");
                        put("description", "That is super title");
                        put("id", "1");
                    }},
                "page", "editSeo", "PageEditSeo"};
    }

    private static Object[] PageEditSeoWrongId()
    {
        return new Object[]{
                new String[][]
                        {
                                {"type", "-1", "type"},
                                {"id", "The selected id is invalid.", "msg"}
                        },
                new HashMap<String, String>()
                {{
                        put("paitle", "WrongTitle");
                        put("id", "111");
                    }},
                "page", "editSeo", "PageEditSeoWrongId"};
    }

    private static Object[] PageEditTitleWrongID()
    {

        return new Object[]{
                new String[][]
                        {
                                {"type", "-1", "type"},
                                {"id", "The selected id is invalid.", "msg"}
                        },
                new HashMap<String, String>()
                {{
                        put("title", "SomeWrongTitle");
                        put("id", "111");
                    }},
                "page", "editTitle", "PageEditTitleWrongID"};

    }

    private static Object[] PageEditTitleNoID()
    {

        return new Object[]{
                new String[][]
                        {
                                {"type", "-1", "type"},
                                {"id", "The id field is required.", "msg"}
                        },
                new HashMap<String, String>()
                {{
                        put("title", "SomeWrongTitle");
                    }},
                "page", "editTitle", "PageEditTitleNoID"};

    }

    private static Object[] PageEditTitleNoTitle()
    {

        return new Object[]{
                new String[][]
                        {
                                {"type", "-1", "type"},
                                {"title", "The title field is required.", "msg"}
                        },
                new HashMap<String, String>()
                {{
                        put("id", "1");
                    }},
                "page", "editTitle", "PageEditTitleNoTitle"};
    }

    private static Object[] PageEditTitle()
    {
        return new Object[]{
                new String[][]
                        {
                                {"type", "1", "type"},
                                {"global", "Page title edited", "msg"},
                                {"title", "ANewTitle", "data"}
                        },
                new HashMap<String, String>()
                {{
                        put("id", "1");
                        put("title", "ANewTitle");
                    }}, "page", "editTitle", "PageEditTitle"};
    }

    private static Object[] PageChangeMargin()
    {
        return new Object[]{
                new String[][]
                        {
                                {"type", "1", "type"},
                                {"global", "Resized", "msg"}
                        },
                new HashMap<String, String>()
                {{
                        put("id", "1");
                        put("margin", "-10");
                    }},
                "page", "changeMargin", "Page change Margin"};
    }

    private static Object[] PageChangeMarginNoId()
    {
        return new Object[]{
                new String[][]
                        {
                                {"type", "-1", "type"},
                                {"id", "The id field is required.", "msg"}
                        },
                new HashMap<String, String>()
                {{
                        put("margin", "-10");
                    }},
                "page", "changeMargin", "Page change Margin No Id"};
    }

    private static Object[] PageMoveNoId()
    {

        {
            return new Object[]{new String[][]
                    {
                            {"type", "-1", "type"},
                            {"id", "The selected id is invalid.", "msg"}
                    },
                    new HashMap<String, String>()
                    {{
                            put("ordering", "1");
                            put("parentPageId", "1");
                            put("id", "222");
                        }}, "page", "move", "PageMoveNoId"};
        }
    }

    private static Object[] PageRemoveWrongId()
    {

        return new Object[]{new String[][]
                {
                        {"type", "-1", "type"},
                        {"id", "The selected id is invalid.", "msg"}
                },
                new HashMap<String, String>()
                {{
                        put("id", "222");
                    }}, "page", "remove", "PageRemoveWrongId"};

    }

    private static Object[] ModuleButtonAdd()
    {
        return new Object[]{new String[][]
                {
                        {"type", "1", "type"},
                        {"global", "Module Saved", "msg"},
                        {"id", "1", "data"},
                        {"ordering", "1", "data"},
                        {"columnId", "1", "data"}
                }, new HashMap<String, String>()
        {{
                put("text ", "Button");
                put("size ", "0");
                put("ordering", "1");
                put("pageId ", "2");
            }}, "ModuleButton", "add", "ModuleButtonAdd"};

    }

    private static Object[] ModuleTitleAdd()
    {
        return new Object[]{new String[][]
                {
                        {"type", "1", "type"},
                        {"global", "Module Saved", "msg"},
                        {"id", "2", "data"},
                        {"ordering", "1", "data"},
                        {"columnId", "2", "data"},
                        {"rowId", "2", "data"}
                }, new HashMap<String, String>()
        {{
                put("text ", "Sample Title");
                put("ordering", "1");
                put("pageId ", "2");
            }}, "ModuleTitle", "add", "ModuleTitleAdd"};

    }

    private static Object[] ModuleTitleAddRowID()
    {
        return new Object[]{new String[][]
                {
                        {"type", "1", "type"},
                        {"global", "Module Saved", null},
                        {"id", "3", null},
                        {"ordering", "1", null},
                        {"columnId", "3", null},
                        {"rowId", "3", null}
                }, new HashMap<String, String>()
        {{
                put("text ", "Sample Title");
                put("ordering", "1");
                put("pageId ", "2");
                put("rowId ", "3");
            }}, "ModuleTitle", "add", "ModuleTitleAddRowID"};
    }

    private static Object[] ModuleParagraphAdd()
    {
        return new Object[]{new String[][]
                {
                        {"type", "1", "type"},
                        {"global", "Module Saved", null},
                        {"id", "4", null},
                        {"ordering", "2", null}
                }, new HashMap<String, String>()
        {{
                put("text ", "Lorem ipsum dolor sit amet, conse ctetur adipisicing elit, sed do eiusmod incididunt ut labore et dolore magna aliqua.");
                put("ordering", "2");
                put("rowId ", "1");
                put("columnId  ", "4");
            }}, "ModuleParagraph", "add", "ModuleParagraphAdd"};
    }

    private static Object[] ModuleFormAdd()
    {
        return new Object[]{new String[][]
                {
                        {"type", "1", "type"},
                        {"global", "Module Saved", null},
                        {"ordering", "1", null}
                }, new HashMap<String, String>()
        {{
                put("ordering", "1");
                put("size", "0");
                put("pageId", "1");
            }}, "ModuleForm", "add", "ModuleFormAdd"};

    }

    private static Object[] ModuleImageAdd()
    {

        return new Object[]{new String[][]
                {
                        {"type", "1", "type"},
                        {"global", "Module Saved", null},
                        {"ordering", "1", null},
                }, new HashMap<String, String>()
        {{
                put("ordering", "1");
                put("size", "0");
                put("pageId", "1");
                put("src", "sample3.jpg");
            }}, "ModuleForm", "add", "ModuleImageAdd"};
    }

    private static Object[] ModuleVideoAdd()
    {
        return new Object[]{new String[][]
                {
                        {"type", "1", "type"},
                        {"global", "Module Saved", null},
                        {"ordering", "1", null}
                }, new HashMap<String, String>()
        {{
                put("ordering", "1");
                put("size", "0");
                put("pageId", "1");
                put("rowId ", "4");
            }}, "ModuleVideo", "add", "ModuleVideoAdd"};
    }

    private static Object[] ColorPaletteAdd()
    {
        return new Object[]{new String[][]
                {
                        {"type", "1", "type"},
                        {"global", "Color Palette Added", null},
                        {"colorCode", "rgb(208, 49, 49)", null}
                }, new HashMap<String, String>()
        {{
                put("colorCode", "rgb(208, 49, 49)");
            }}, "ColorPalette", "add", "ColorPaletteAdd"};
    }

    private static Object[] ColorPaletteAddRecent()
    {
        return new Object[]{new String[][]
                {
                        {"type", "1", "type"},
                        {"global", "Recent Color updated", null},
                        {"colorCode", "#d03131", null},
                }, new HashMap<String, String>()
        {{
                put("colorCode", "%23d03131");
            }}, "ColorPalette", "addRecentColor", "ColorPaletteAddRecent"};
    }
}
