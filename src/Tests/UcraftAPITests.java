package Tests;

import Core.ApiPrefix;
import Core.Component;
import Utils.*;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.AfterClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.util.HashMap;

/**
 * Created by Admin on 24.09.2015.
 */

public class UcraftAPITests
{
    private WebDriver driver;
    private String BaseDir= System.getProperty("basedir"+ File.separator,"");
    @BeforeClass
    public void SetUp() throws Exception
    {
        DesiredCapabilities caps = DesiredCapabilities.phantomjs();
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, BaseDir+Configuration.DriverPath());
        driver = new PhantomJSDriver(caps);
        System.out.println("================================");
        System.out.println("---------Clearing Data Base--------");
        System.out.println("------please wait around 15 sec.---");
        System.out.println("================================");
        System.out.flush();
        ApiUtils.ClearDb(driver);
        Thread.sleep(15000);
        System.out.println("Clearing done");
        System.out.println("Starting tests, please wait...");
    }
    @AfterClass
    public void TearDown ()
    {
        if (driver != null)
            driver.quit();
    }

    @Test (dataProvider = "dynamic", dataProviderClass = TestDataProvider.class)
    public void DynamicCalls (String request, String expected, String testName)
    {
        Reporter.log("Making dynamic test: <<<" + testName + ">>>");
        Reporter.log("Sending request ->>> " + request);

        String resp = ApiUtils.GetResponse(driver,request);
        Reporter.log("Getting response <<<- " + resp);

        Assert.assertTrue(resp.contains(expected.replace("{", "").replace("}", "")), "In response data (" + expected + ") not found");
        Reporter.log("Response OK!!");
    }

    @Test (dataProvider="apitests", dataProviderClass = TestDataProvider.class)
    public void TestEngine(String[][] expected, HashMap<String,String> request, String entity, String method, String testName)
            throws Exception
    {
        Reporter.log("[Test started] " + entity + "." + method + "("+testName+")");
        String jsonText = JSONObject.toJSONString(request);

        Reporter.log("Sending request ->>> " + jsonText);
        String response = ApiUtils.GetResponse(driver,Configuration.BaseUrl, ApiPrefix.Api, Component.Default, entity, method,jsonText);

        JSONObject actual = (JSONObject) new JSONParser().parse(response);
        Reporter.log("Getting response <<<- " + actual);
        AssertResponse(expected, actual);
        Reporter.log("[Test finished] " + entity + "." + method+ "("+testName+")");
        Reporter.log("Response OK!!");
    }

    private void AssertResponse (String[][] expected, JSONObject actual)
    {
        for (String[] keys : expected) {
            Reporter.log("\nExpected : " + keys[0] + ":" + keys[1]);
            String found = JTokenFinder.Find(actual,keys[0],keys[2]);
            if (found == null) Assert.fail("Incorrect responce! No " + keys[0] + " found");
            Reporter.log("Found : " + keys[0] + ":" + found);
            Assert.assertEquals(keys[1], found, String.format("Expected '%s : %s', but actual was %s",keys[0], keys[1], found));
        }

    }
}
