package Tests;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Admin on 30.09.2015.
 */
public class Reporter {
    public static void log(String s) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("H:mm:ss:SSS");
        sdf.format(cal.getTime());
        org.testng.Reporter.log("[" + sdf.format(cal.getTime()) + "]: " + s + " <br></br>");
    }

}
