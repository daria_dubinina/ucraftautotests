package Executor;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DefaultLogger;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class AntExecutor {
    private File buildFile;
    private Project project;
    private ProjectHelper helper;

    private Map<String, String> properties = new HashMap<>();

    public AntExecutor(String currentDir) {
        buildFile = new File(new File(currentDir).getAbsolutePath() + File.separator + "build.xml");

        project = new Project();
        project.setUserProperty("ant.file", buildFile.getAbsolutePath());

        DefaultLogger consoleLogger = new DefaultLogger();
        consoleLogger.setErrorPrintStream(System.err);
        consoleLogger.setOutputPrintStream(System.out);
        consoleLogger.setMessageOutputLevel(Project.MSG_INFO);
        project.addBuildListener(consoleLogger);

        helper = ProjectHelper.getProjectHelper();
        project.addReference("ant.projectHelper", helper);
    }

    public void runTarget(String targetName) {
        System.out.println("Calling Ant target '" + targetName + "'...");
        try {
            project.init();
            helper.parse(project, buildFile);

            for (String property: properties.values()) {
                project.setProperty(property, properties.get(property));
            }

            project.fireBuildStarted();
            project.executeTarget(targetName);
            project.fireBuildFinished(null);
        } catch (BuildException e) {
            project.fireBuildFinished(e);
        }
    }
}
