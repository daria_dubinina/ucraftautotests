package Executor;

import java.io.File;

public class Main {
    public static void main(String[] args) throws Exception {
        AntExecutor executor = new AntExecutor(new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParentFile().getPath());
        executor.runTarget("testAll");
    }
}
