package Utils;

import Core.ApiPrefix;
import Core.Component;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ApiUtils
{

    public static String Format(String url, ApiPrefix apiPrefix, Component component, String entity, String method, String json)
    {
        String formatted = String.join("/", url, apiPrefix.toString(), component.toString(), entity, method);
        formatted += "?data=" + json;
        return formatted;
    }
    public static String GetResponse(WebDriver driver, String url, ApiPrefix apiPrefix, Component component, String entity, String method, String json)
    {
        String formatted = Format(url, apiPrefix, component, entity, method, json);
        return GetResponse(driver,formatted);
    }
    public static String GetResponse(WebDriver driver, String request)
    {
        driver.navigate().to(request);
        return driver.findElement(By.xpath("//html")).getText();
    }
    public static void ClearDb(WebDriver driver)
    {
        String  uri = Configuration.BaseUrl + "/m/db";
        driver.navigate().to(uri);
    }
}

