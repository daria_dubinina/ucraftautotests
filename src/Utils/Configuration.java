package Utils;

import java.io.File;

/**
 * Created by Admin on 25.09.2015.
 */
public class Configuration
{
    public static String BaseUrl = System.getProperty("baseurl", "http://testing-site.ucraft.me");
    private static String BaseDir = "lib"+File.separatorChar + "driver" +File.separatorChar;
    private static String DriverNameWin = "phantomjs.exe";
    private static String DriverNameMac = "phantomjs";
    public static OS OsName()
    {
        String name = System.getProperty("os.name").toLowerCase();
        if (name.contains("windows"))
            return OS.Windows;
        else if (name.contains("mac"))
            return OS.MacOS;
        else
            throw new InternalError("Name [" + name + "] not recognized");
    }
    public static String DriverPath()
    {
        switch(OsName())
        {
            case Windows:
                return BaseDir + DriverNameWin;
            case MacOS:
                return BaseDir + DriverNameMac;
            default:
                return null;
        }
    }
}

enum OS
{
    Windows, MacOS
}