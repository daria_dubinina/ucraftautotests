package Utils;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.util.HashMap;
import java.util.Iterator;


/**
 * Created by Admin on 28.09.2015.
 */
public class JTokenFinder
{
    public static String Find (JSONObject root, String key, String parent)
    {
        for(Object childKey:((HashMap)root).keySet())
        {
            Object foundKey = root.get(childKey);
            if (foundKey != null && foundKey instanceof JSONObject && (parent==null || parent.equals(childKey.toString()))) {
                String found = Find((JSONObject) foundKey, key, null);
                if (found != null)
                    return found;
            }

                if (childKey != null && childKey.equals(key)) {
                    Object rez = root.get(childKey);
                    return rez == null ? "null" : rez.toString();
                }
        }
        return null;
    }
}
