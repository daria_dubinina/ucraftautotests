package Utils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Admin on 24.09.2015.
 */
public class TestDataReader
{
    private static final String TESTDATA_NAME = "DataProvider.xml";

    private String BaseDir= System.getProperty("basedir"+ File.separator,"");
    private static final String ROOT = "Tests";
    private static final String NODE_TAG = "TestData";
    private static final String API_PREFIX_TAG = "ApiPrefix";
    private static final String COMPONENT_TAG = "Component";
    private static final String ENTITY_TAG = "Entity";
    private static final String METHOD_TAG = "Method";
    private static final String DATA_TAG = "Data";
    private static final String EXPECTED_TAG = "Expected";


    List<TestData> testData = new ArrayList<>();

    public TestDataReader() {
        String filePath = null;
        List<Node> testNodes;
        try {
            filePath = Paths.get(BaseDir+TESTDATA_NAME).toString();
            Document datapool = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(filePath);
            datapool.getDocumentElement().normalize();

            testNodes =  FilterNodeList(datapool.getChildNodes(), ROOT);
            testNodes = FilterNodeList(testNodes.get(0).getChildNodes(),NODE_TAG);
            for (Node allnodes : testNodes) {
                TestData data = new TestData();
                for (Node node : TransformNodeList(allnodes.getChildNodes())) {
                    switch (node.getNodeName()) {
                        case API_PREFIX_TAG:
                            data.ApiPrefix = node.getFirstChild().getNodeValue();
                            break;
                        case COMPONENT_TAG:
                            data.Component = node.getFirstChild().getNodeValue();
                            break;
                        case ENTITY_TAG:
                            data.Entity = node.getFirstChild().getNodeValue();
                            break;
                        case METHOD_TAG:
                            data.Method = node.getFirstChild().getNodeValue();
                            break;
                        case DATA_TAG:
                            data.Data = node.getFirstChild().getNodeValue();
                            break;
                        case EXPECTED_TAG:
                            data.Expected = node.getFirstChild().getNodeValue();
                            break;
                    }
                }
                testData.add(data);
            }

        } catch (Exception e) {
            System.err.println("Unable to read Test Data file: " + filePath + "! " + e.getMessage());
            e.printStackTrace();
        }
    }

    public List<TestData> GetTestData()
    {
        return testData;
    }

    private List<Node> TransformNodeList(NodeList list) {
        List<Node> nodes = new ArrayList<>();
        for (int i = 0; i < list.getLength(); i++) {
            nodes.add(list.item(i));
        }

        return nodes;
    }

    private List<Node> FilterNodeList(NodeList list, String tagFilter) {
        List<Node> nodes = TransformNodeList(list);
        List<Node> filteredNodes = nodes.stream().filter(n -> {
            if (n.getNodeType() == Node.ELEMENT_NODE) {
                return ((Element) n).getTagName().equals(tagFilter);
            }
            return false;
        }).collect(Collectors.toList());

        return filteredNodes;
    }
}
